package com.yiang.yiangus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author HeZhuo
 * @date 2020/12/3
 */
@SpringBootApplication
public class YiangApp {

    public static void main(String[] args) {
        //druid的一个BUG，设置druid的参数禁止使用ping的方法，而是采用select 1
        System.setProperty("druid.mysql.usePingMethod", "false");
        SpringApplication.run(YiangApp.class, args);
    }
}
