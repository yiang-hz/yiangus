package com.yiang.yiangus.base.result.code;

import java.io.Serializable;

/**
 * @author HeZhuo
 * @date 2020-09-25
 */
public interface IResultCode extends Serializable {

    /**
     * 获取返回信息
     * @return String message
     */
    String getMessage();

    /**
     * 获取状态码
     * @return Int code
     */
    int getCode();
}