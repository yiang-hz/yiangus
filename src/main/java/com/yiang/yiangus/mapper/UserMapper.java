package com.yiang.yiangus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yiang.yiangus.model.User;
import org.springframework.stereotype.Repository;

/**
 * @author HeZhuo
 * @date 2020/12/4
 */
@Repository
public interface UserMapper extends BaseMapper<User> {
}
