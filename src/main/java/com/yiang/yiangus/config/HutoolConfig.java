package com.yiang.yiangus.config;

import cn.hutool.extra.spring.EnableSpringUtil;
import org.springframework.context.annotation.Configuration;

/**
 * @author HeZhuo
 * @date 2020/12/4
 */
@Configuration
@EnableSpringUtil
public class HutoolConfig {
}
