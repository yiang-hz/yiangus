package com.yiang.yiangus.config;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.net.NetUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.Data;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author HeZhuo
 * @date 2021/4/1
 */
@Component
@Aspect
public class RequestLogAspect {
    private final static Logger LOGGER = LoggerFactory.getLogger(RequestLogAspect.class);

    @Pointcut("execution(* com.yiang.yiangus.controller..*(..))")
    public void requestServer() {
    }

    @Around("requestServer()")
    public Object doAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        long start = System.currentTimeMillis();
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        RequestInfo requestInfo = new RequestInfo();
        if (!ObjectUtils.isEmpty(attributes) && !ObjectUtils.isEmpty(attributes.getRequest())) {
            HttpServletRequest request = attributes.getRequest();
            requestInfo.setIp(request.getRemoteAddr());
            requestInfo.setUrl(request.getRequestURL().toString());
            requestInfo.setHttpMethod(request.getMethod());
        }
        Object result = proceedingJoinPoint.proceed();
        requestInfo.setClassMethod(String.format("%s.%s", proceedingJoinPoint.getSignature().getDeclaringTypeName(),
                proceedingJoinPoint.getSignature().getName()));
        requestInfo.setRequestParams(getRequestParamsByProceedingJoinPoint(proceedingJoinPoint));
        requestInfo.setResult(result);
        if (!ObjectUtils.isEmpty(result)) {
            JSONObject jsonObject = JSONUtil.parseObj(result);
            Object data = jsonObject.get("data");
            if (!ObjectUtils.isEmpty(data)) {
                if (data.toString().length() > 200) {
                    jsonObject.remove("data");
                    requestInfo.setResult(jsonObject);
                }
            }
        }
        requestInfo.setTimeCost(System.currentTimeMillis() - start);
        LOGGER.info("Request Info      : {}", JSONUtil.toJsonStr(requestInfo));
        return result;
    }

    @AfterThrowing(pointcut = "requestServer()", throwing = "e")
    public void doAfterThrow(JoinPoint joinPoint, RuntimeException e) {
        try {
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            RequestErrorInfo requestErrorInfo = new RequestErrorInfo();
            if (!ObjectUtils.isEmpty(attributes) && !ObjectUtils.isEmpty(attributes.getRequest())) {
                HttpServletRequest request = attributes.getRequest();
                requestErrorInfo.setIp(request.getRemoteAddr());
                requestErrorInfo.setUrl(request.getRequestURL().toString());
                requestErrorInfo.setHttpMethod(request.getMethod());
            }
            requestErrorInfo.setClassMethod(String.format("%s.%s", joinPoint.getSignature().getDeclaringTypeName(),
                    joinPoint.getSignature().getName()));
            requestErrorInfo.setRequestParams(getRequestParamsByJoinPoint(joinPoint));
            StackTraceElement[] stackTrace = e.getStackTrace();
            // 转换成json
            JSONObject jsonObject = new JSONObject();
            if (!ObjectUtils.isEmpty(stackTrace)) {
                StackTraceElement stackTraceElement = stackTrace[0];
                jsonObject = JSONUtil.parseObj(JSONUtil.toJsonStr(stackTraceElement));
                // 转换成json
                jsonObject.set("errorContent", e.getMessage());
                jsonObject.set("createTime", DateUtil.date());
                jsonObject.setDateFormat(DatePattern.NORM_DATETIME_PATTERN);
                jsonObject.set("messageId", IdUtil.fastSimpleUUID());
                // 获取IP地址
                jsonObject.set("serverIp", NetUtil.getLocalhostStr());
            }
            requestErrorInfo.setErrorMessage(jsonObject);
            LOGGER.info("Error Request Info      : {}", JSONUtil.toJsonStr(requestErrorInfo));
        } catch (Exception ignored) {

        }
    }

    /**
     * 获取入参
     * @param proceedingJoinPoint 入参
     *
     * @return 返回
     * */
    private Map<String, Object> getRequestParamsByProceedingJoinPoint(ProceedingJoinPoint proceedingJoinPoint) {
        //参数名
        String[] paramNames = ((MethodSignature)proceedingJoinPoint.getSignature()).getParameterNames();
        //参数值
        Object[] paramValues = proceedingJoinPoint.getArgs();

        return buildRequestParam(paramNames, paramValues);
    }

    private Map<String, Object> getRequestParamsByJoinPoint(JoinPoint joinPoint) {
        try {
            //参数名
            String[] paramNames = ((MethodSignature)joinPoint.getSignature()).getParameterNames();
            //参数值
            Object[] paramValues = joinPoint.getArgs();

            return buildRequestParam(paramNames, paramValues);
        } catch (Exception e) {
            return new HashMap<>();
        }
    }

    private Map<String, Object> buildRequestParam(String[] paramNames, Object[] paramValues) {
        try {
            Map<String, Object> requestParams = new HashMap<>(paramNames.length);
            for (int i = 0; i < paramNames.length; i++) {
                Object value = paramValues[i];

                //如果是文件对象
                if (value instanceof MultipartFile) {
                    MultipartFile file = (MultipartFile) value;
                    //获取文件名
                    value = file.getOriginalFilename();
                }

                requestParams.put(paramNames[i], value);
            }

            return requestParams;
        } catch (Exception e) {
            return new HashMap<>(1);
        }
    }

    @Data
    public static class RequestInfo {
        private String ip;
        private String url;
        private String httpMethod;
        private String classMethod;
        private Object requestParams;
        private Object result;
        private Long timeCost;
    }

    @Data
    public static class RequestErrorInfo {
        private String ip;
        private String url;
        private String httpMethod;
        private String classMethod;
        private Object requestParams;
        private JSONObject errorMessage;
    }
}

