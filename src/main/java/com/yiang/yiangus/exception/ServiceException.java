package com.yiang.yiangus.exception;

import com.yiang.yiangus.base.result.code.IResultCode;
import com.yiang.yiangus.base.result.code.ResultCode;

/**
 * @author HeZhuo
 * @date 2020/12/8
 */
@SuppressWarnings("unused")
public class ServiceException extends RuntimeException {
    private static final long serialVersionUID = 2359767895161832954L;
    private final IResultCode resultCode;

    public ServiceException(String message) {
        super(message);
        this.resultCode = ResultCode.FAILURE;
    }

    public ServiceException(IResultCode resultCode) {
        super(resultCode.getMessage());
        this.resultCode = resultCode;
    }

    public ServiceException(IResultCode resultCode, Throwable cause) {
        super(cause);
        this.resultCode = resultCode;
    }

    @Override
    public String toString() {
        //return MessageFormat.format("{0}:[{1}]", this.resultCode.getCode(), this.resultCode.getMessage());
        return super.toString();
    }

    @Override
    public Throwable fillInStackTrace() {
//        return this;
        return super.fillInStackTrace();
    }

    public IResultCode getResultCode() {
        return this.resultCode;
    }
}