package com.yiang.yiangus.exception;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.net.NetUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.yiang.yiangus.base.result.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;


/**
 * @author HeZhuo
 * @date 2021/3/26
 */
@ControllerAdvice
@Slf4j
public class PoliceGlobalExceptionHandler {

    @Value("${spring.application.name}")
    private String serverIdName;

    @Value("${server.port}")
    private String serverPort;

    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public R<String> errorResult(HttpServletRequest request, RuntimeException ex) {
        // 采用全局捕获异常 拦截系统中的错误，返回友好的提示给客户端
        Map<String, String[]> parameterMap = request.getParameterMap();
        StackTraceElement[] stackTrace = ex.getStackTrace();
        StackTraceElement stackTraceElement = stackTrace[0];
        // 转换成json
        JSONObject jsonObject = JSONUtil.parseObj(JSONUtil.toJsonStr(stackTraceElement));
        // 新增参数内容
        jsonObject.set("parameterContent", parameterMap);
        // 新增错误日志
        jsonObject.set("errorContent", ex.getMessage());
        jsonObject.set("serviceId", serverIdName);
        jsonObject.set("createTime", DateUtil.date());
        jsonObject.setDateFormat(DatePattern.NORM_DATETIME_PATTERN);
        jsonObject.set("messageId", serverIdName + "-" + UUID.randomUUID().toString());
        // 获取IP地址
        jsonObject.set("serverIp", NetUtil.getLocalhostStr() + serverPort);
        log.error(jsonObject.toString());
        return R.fail(ex.getMessage());
    }
    @ExceptionHandler(ServiceException.class)
    @ResponseBody
    public R<String> errorResultE(HttpServletRequest request, ServiceException ex) {
        // 采用全局捕获异常 拦截系统中的错误，返回友好的提示给客户端
        Map<String, String[]> parameterMap = request.getParameterMap();
        StackTraceElement[] stackTrace = ex.getStackTrace();
        // 转换成json
        JSONObject jsonObject = new JSONObject();
        if (!ObjectUtils.isEmpty(stackTrace)) {
            StackTraceElement stackTraceElement = stackTrace[0];
            // 转换成json
            jsonObject = JSONUtil.parseObj(JSONUtil.toJsonStr(stackTraceElement));
        }
        // 新增参数内容
        jsonObject.set("parameterContent", parameterMap);
        // 新增错误日志
        jsonObject.set("errorContent", ex.getMessage());
        jsonObject.set("serviceId", serverIdName);
        jsonObject.set("createTime", DateUtil.date());
        jsonObject.setDateFormat(DatePattern.NORM_DATETIME_PATTERN);
        jsonObject.set("messageId", IdUtil.fastSimpleUUID());
        // 获取IP地址
        jsonObject.set("serverIp", NetUtil.getLocalhostStr() + serverPort);
        log.error(jsonObject.toString());
        ex.printStackTrace();
        return R.fail(ex.getMessage());
    }

    public static void main(String[] args) {
        System.out.println(IdUtil.fastSimpleUUID());
    }
}
