package com.yiang.yiangus.core;

import cn.hutool.core.convert.Convert;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.util.LinkedCaseInsensitiveMap;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @date 2020-09-23
 * @author HeZhuo
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Kv extends LinkedCaseInsensitiveMap<Object> {

    public static Kv create() {
        return new Kv();
    }

    @SuppressWarnings("rawtypes,unused")
    public static HashMap newMap() {
        return new HashMap<>(16);
    }

    public Kv set(String attr, Object value) {
        this.put(attr, value);
        return this;
    }

    @SuppressWarnings("rawtypes,unchecked,unused")
    public Kv setAll(Map map) {
        this.putAll(map);
        return this;
    }

    @SuppressWarnings("unused")
    public Kv setIgnoreNull(String attr, Object value) {
        if (null != attr && null != value) {
            this.set(attr, value);
        }

        return this;
    }

    @SuppressWarnings("unused")
    public Object getObj(String key) {
        return super.get(key);
    }

    @SuppressWarnings("unused")
    public String getStr(String attr) {
        return Convert.toStr(this.get(attr));
    }

    @SuppressWarnings("unused")
    public Integer getInt(String attr) {
        return Convert.toInt(this.get(attr), -1);
    }

    @SuppressWarnings("unused")
    public Long getLong(String attr) {
        return Convert.toLong(this.get(attr), -1L);
    }

    @SuppressWarnings("unused")
    public Float getFloat(String attr) {
        return Convert.toFloat(this.get(attr));
    }

    @SuppressWarnings("unused")
    public Double getDouble(String attr) {
        return Convert.toDouble(this.get(attr));
    }

    @SuppressWarnings("unused")
    public Boolean getBool(String attr) {
        return Convert.toBool(this.get(attr));
    }

    @SuppressWarnings("unused")
    public byte[] getBytes(String attr) {
        return Convert.toPrimitiveByteArray(this.get(attr));
    }

    @SuppressWarnings("unused")
    public Date getDate(String attr) {
        return Convert.toDate(this.get(attr));
    }

    @SuppressWarnings("unused")
    public Time getTime(String attr) {
        return (Time) this.get(attr);
    }

    @SuppressWarnings("unused")
    public Timestamp getTimestamp(String attr) {
        return (Timestamp) this.get(attr);
    }

    @SuppressWarnings("unused")
    public Number getNumber(String attr) {
        return Convert.toNumber(this.get(attr));
    }

    @Override
    public Kv clone() {
        return (Kv) super.clone();
    }
}