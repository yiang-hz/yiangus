package com.yiang.yiangus.core;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import java.util.Map;

import static cn.hutool.core.date.DatePattern.NORM_DATETIME_PATTERN;

/**
 * @date 2020-09-23
 * @author HeZhuo
 */
public class SqlKeyword {

    private static final String SQL_REGEX = "'|%|--|insert|delete|update|select|count|group|" +
            "union|drop|truncate|alter|grant|execute|exec|xp_cmdshell|call|declare|sql";
    private static final String SHORT_EQUAL = "_eq";
    private static final String EQUAL = "_equal";
    private static final String SHORT_NOT_EQUAL = "_noteq";
    private static final String NOT_EQUAL = "_notequal";
    private static final String LIKE = "_like";
    private static final String LIKE_LEFT = "_likeleft";
    private static final String LIKE_RIGHT = "_likeright";
    private static final String NOT_LIKE = "_notlike";
    private static final String GE = "_ge";
    private static final String LE = "_le";
    private static final String GT = "_gt";
    private static final String LT = "_lt";
    private static final String DATE_GE = "_datege";
    private static final String DATE_GT = "_dategt";
    private static final String DATE_EQUAL = "_dateequal";
    private static final String DATE_LT = "_datelt";
    private static final String DATE_LE = "_datele";
    private static final String IS_NULL = "_null";
    private static final String NOT_NULL = "_notnull";
    private static final String IGNORE = "_ignore";

    public SqlKeyword() {
    }

    public static void buildCondition(Map<String, Object> query, QueryWrapper<?> qw) {
        if (!ObjectUtil.isEmpty(query)) {
            query.forEach((k, v) -> {
                if (!ObjectUtil.hasEmpty(k, v) && !k.endsWith(IGNORE)) {
                    if (k.endsWith(EQUAL)) {
                        qw.eq(getColumn(k, EQUAL), v);
                    } else if (k.endsWith(SHORT_EQUAL)) {
                        qw.ne(getColumn(k, EQUAL), v);
                    } else if (k.endsWith(SHORT_NOT_EQUAL)) {
                        qw.ne(getColumn(k, NOT_EQUAL), v);
                    } else if (k.endsWith(NOT_EQUAL)) {
                        qw.ne(getColumn(k, NOT_EQUAL), v);
                    } else if (k.endsWith(LIKE_LEFT)) {
                        qw.likeLeft(getColumn(k, LIKE_LEFT), v);
                    } else if (k.endsWith(LIKE_RIGHT)) {
                        qw.likeRight(getColumn(k, LIKE_RIGHT), v);
                    } else if (k.endsWith(NOT_LIKE)) {
                        qw.notLike(getColumn(k, NOT_LIKE), v);
                    } else if (k.endsWith(GE)) {
                        qw.ge(getColumn(k, GE), v);
                    } else if (k.endsWith(LE)) {
                        qw.le(getColumn(k, LE), v);
                    } else if (k.endsWith(GT)) {
                        qw.gt(getColumn(k, GT), v);
                    } else if (k.endsWith(LT)) {
                        qw.lt(getColumn(k, LT), v);
                    } else if (k.endsWith(DATE_GE)) {
                        qw.ge(getColumn(k, DATE_GE), DateUtil.parse(String.valueOf(v), NORM_DATETIME_PATTERN));
                    } else if (k.endsWith(DATE_GT)) {
                        qw.gt(getColumn(k, DATE_GT), DateUtil.parse(String.valueOf(v), NORM_DATETIME_PATTERN));
                    } else if (k.endsWith(DATE_EQUAL)) {
                        qw.eq(getColumn(k, DATE_EQUAL), DateUtil.parse(String.valueOf(v), NORM_DATETIME_PATTERN));
                    } else if (k.endsWith(DATE_LE)) {
                        qw.le(getColumn(k, DATE_LE), DateUtil.parse(String.valueOf(v), NORM_DATETIME_PATTERN));
                    } else if (k.endsWith(DATE_LT)) {
                        qw.lt(getColumn(k, DATE_LT), DateUtil.parse(String.valueOf(v), NORM_DATETIME_PATTERN));
                    } else if (k.endsWith(IS_NULL)) {
                        qw.isNull(getColumn(k, IS_NULL));
                    } else if (k.endsWith(NOT_NULL)) {
                        qw.isNotNull(getColumn(k, NOT_NULL));
                    } else {
                        qw.like(getColumn(k, LIKE), v);
                    }
                }
            });
        }
    }

    private static String getColumn(String column, String keyword) {
        return StringUtil.humpToUnderline(StringUtil.removeSuffix(column, keyword));
    }

    public static String filter(String param) {

        if (null == param) {
            return StrUtil.EMPTY;
        }

        return param.replaceAll("(?i)" + SQL_REGEX, StrUtil.EMPTY);
    }
}