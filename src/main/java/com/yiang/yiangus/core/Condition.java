package com.yiang.yiangus.core;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.extra.cglib.CglibUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * 操作获取方法类
 * @author HeZhuo
 * @date 2020/9/22
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Condition {

    public static <T> IPage<T> getPage(Query query) {

        Page<T> page = new Page<>(Convert.toLong(query.getCurrent(), 1L),
                Convert.toLong(query.getSize(), 10L));

        page.addOrder(OrderItem.ascs(
                Convert.toStrArray(SqlKeyword.filter(query.getAscs()))
        ));

        page.addOrder(OrderItem.descs(
                Convert.toStrArray(SqlKeyword.filter(query.getDescs()))
        ));

        return page;
    }

    @SuppressWarnings("unused")
    public static <T> QueryWrapper<T> getQueryWrapper(Map<String, Object> query, Class<T> clazz) {

        Kv exclude = Kv.create()
                .set("Yiang-Auth", "Yiang-Auth")
                .set("current", "current")
                .set("size", "size")
                .set("ascs", "ascs")
                .set("descs", "descs");

        return getQueryWrapper(query, exclude, clazz);
    }

    public static <T> QueryWrapper<T> getQueryWrapper(Map<String, Object> query, Map<String, Object> exclude, Class<T> clazz) {

        exclude.forEach((k, v) -> query.remove(k));

        QueryWrapper<T> qw = new QueryWrapper<>();
        qw.setEntity(ReflectUtil.newInstance(clazz));
        SqlKeyword.buildCondition(query, qw);

        return qw;
    }

    public static <T, K> IPage<T> castPage(IPage<K> iPage, Class<T> nClass) {
        return iPage.convert(i -> CglibUtil.copy(i, nClass));
    }

}
