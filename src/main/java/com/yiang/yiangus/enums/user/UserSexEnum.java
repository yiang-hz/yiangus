package com.yiang.yiangus.enums.user;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author HeZhuo
 * @date 2021/1/12
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
@ApiModel("用户性别枚举类")
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum UserSexEnum {

    //状态信息
    MAN(0, "男"),
    WOMAN(1, "女");

    //    @JsonValue
    @EnumValue
    @ApiModelProperty("状态Id编码Code")
    private Integer code;

    @ApiModelProperty("状态名称说明")
    private String name;

}
