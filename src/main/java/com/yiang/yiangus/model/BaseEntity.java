package com.yiang.yiangus.model;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author HeZhuo
 * @date 2020/12/4
 */
@Data
@ApiModel("基础实体类")
@NoArgsConstructor
@AllArgsConstructor
public class BaseEntity implements Serializable {

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;
}
