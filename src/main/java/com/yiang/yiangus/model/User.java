package com.yiang.yiangus.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yiang.yiangus.enums.user.UserSexEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.ibatis.type.Alias;

/**
 * @author HeZhuo
 * @date 2020/12/4
 */
@Data
@Alias("User")
@ApiModel("用户实体类")
@TableName("user")
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class User extends BaseEntity {

    @ApiModelProperty("主键ID")
    @TableId(type = IdType.ASSIGN_ID)
    private String userId;

    @ApiModelProperty("用户名称")
    @TableField("user_name")
    private String userName;

    @TableField("sex")
    @ApiModelProperty("性别")
    private UserSexEnum sex;

}
