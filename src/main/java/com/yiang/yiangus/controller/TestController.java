package com.yiang.yiangus.controller;

import com.yiang.yiangus.base.result.R;
import com.yiang.yiangus.exception.ServiceException;
import com.yomahub.tlog.core.annotation.TLogAspect;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HeZhuo
 * @date 2020/12/8
 */
@RestController
@RequestMapping("/test")
@Slf4j
public class TestController {

    @TLogAspect({"id"})
    @GetMapping("/exception")
    @SuppressWarnings("all")
    public R<?> testException(String name) {
        log.info("这是第一条日志");
        log.info("这是第二条日志");
        log.info("这是第三条日志");
        new Thread(() -> log.info("这是异步日志")).start();
        if ("exception".equals(name)) {
            throw new ServiceException("抛出异常了！");
        }
        return R.data(name);
    }

    @GetMapping("/exception2")
    @SuppressWarnings("all")
    public R<?> testException2(String name) {
        log.info("这是第一条日志");
        log.info("这是第二条日志");
        log.info("这是第三条日志");
        new Thread(() -> log.info("这是异步日志")).start();
        if ("exception".equals(name)) {
            throw new ServiceException("抛出异常了！");
        }
        return R.data(name);
    }
}
