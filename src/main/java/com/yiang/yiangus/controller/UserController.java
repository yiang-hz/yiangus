package com.yiang.yiangus.controller;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.yiang.yiangus.base.result.R;
import com.yiang.yiangus.base.result.code.ResultCode;
import com.yiang.yiangus.core.Condition;
import com.yiang.yiangus.core.Query;
import com.yiang.yiangus.dto.user.UserQueryDTO;
import com.yiang.yiangus.model.User;
import com.yiang.yiangus.server.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author HeZhuo
 * @date 2020/12/4
 */
@RestController
@AllArgsConstructor
@Api(tags = "用户接口")
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @PostMapping("/save")
    @ApiOperation(value = "新增User接口")
    public R<?> save(@RequestBody User user) {
        return R.status(userService.save(user));
    }

    @PostMapping("/remove")
    @ApiOperation(value = "删除User接口，根据主键ID删除，多个ID使用英文逗号分割")
    public R<?> remove(@RequestParam String ids) {

        if (ObjectUtils.isEmpty(ids)) {
            return R.fail(ResultCode.PARAM_MISS);
        }

        return R.status(userService.removeByIds(Convert.toList(String.class, ids)));
    }

    @PostMapping("/update")
    @ApiOperation(value = "修改User接口")
    public R<?> update(@RequestBody User user) {
        return R.status(userService.updateById(user));
    }

    @GetMapping("/detail/{id}")
    @ApiOperation(value = "单个查询User接口")
    public R<User> detail(@PathVariable("id") Long id) {
        return R.data(userService.getById(id));
    }

    @GetMapping("/list")
    @ApiOperation(value = "用户集合")
    public R<List<User>> list() {
        return R.data(userService.list());
    }

    @GetMapping("/page")
    @ApiOperation(value = "分页查询User接口")
    public R<IPage<UserQueryDTO>> page(Query query) {

        IPage<User> userPage = userService.page(Condition.getPage(query),
                Wrappers.emptyWrapper());

        IPage<UserQueryDTO> resultPage = Condition.castPage(
                userPage, UserQueryDTO.class
        );

        return R.data(resultPage);
    }

}
