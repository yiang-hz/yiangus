package com.yiang.yiangus.controller;

import cn.hutool.extra.spring.SpringUtil;
import com.alibaba.druid.support.json.JSONUtils;
import com.yiang.yiangus.server.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HeZhuo
 * @date 2020/12/3
 */
@Api(tags = "首页模块")
@RestController
public class IndexController {

    @ApiImplicitParam(name = "name", value = "姓名", required = true)
    @ApiOperation(value = "向客人问好")
    @GetMapping("/sayHi")
    public ResponseEntity<String> sayHi(@RequestParam(value = "name")String name) {
        UserService userService = SpringUtil.getBean("userService");
        try {
            userService.sout(name);
        } finally {
            System.out.println("1");
        }
        return ResponseEntity.ok("Hi:" + JSONUtils.toJSONString(name));
    }
}
