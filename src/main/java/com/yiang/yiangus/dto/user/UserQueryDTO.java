package com.yiang.yiangus.dto.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author HeZhuo
 * @date 2021/1/12
 */
@ApiModel("用户查询接口参数")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserQueryDTO {

    @ApiModelProperty("用户名称")
    private String userName;

    @ApiModelProperty("性别")
    private String sex;
}
