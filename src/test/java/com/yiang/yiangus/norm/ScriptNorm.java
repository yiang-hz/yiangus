package com.yiang.yiangus.norm;

import cn.hutool.script.ScriptUtil;

/**
 * 脚本语言测试类，用于做自定义的状态判断
 * @author HeZhuo
 * @date 2020/12/8
 */
public class ScriptNorm {

    public static void main(String[] args) {
        ScriptUtil.eval("print('Script test!');");
        Object eval = ScriptUtil.eval("1==1");
        System.out.println(eval);

        String symbol = "==";
        int compareA = 1;
        int compareB = 1;
        System.out.println(ScriptUtil.eval(compareA + symbol + compareB));
    }
}
