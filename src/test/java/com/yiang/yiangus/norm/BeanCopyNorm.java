package com.yiang.yiangus.norm;

import cn.hutool.extra.cglib.CglibUtil;
import com.yiang.yiangus.model.User;

/**
 * @author HeZhuo
 * @date 2020/12/8
 */
public class BeanCopyNorm {

    public static void main(String[] args) {
        //使用CglibUtil的性能会比BeanUtil的性能要好
        User user = new User();
        user.setUserId("1");
        user.setUserName("何卓");
        //1. 方法1直接传递类Class返回复制对象
        User copy = CglibUtil.copy(user, User.class);
        System.out.println(copy);
        //2. 直接传入对象赋值
        User copy2 = new User();
        CglibUtil.copy(user, copy2);
        System.out.println(copy2);
    }
}
