package com.yiang.yiangus.test;

import cn.hutool.extra.spring.SpringUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author HeZhuo
 * @date 2020/12/8
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestSpringClass {

    @Test
    public void test() {
        System.out.println(SpringUtil.getBean("userService").toString());
    }
}
