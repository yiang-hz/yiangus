package com.yiang.yiangus.test;

import cn.hutool.core.thread.ConcurrencyTester;

/**
 * @author HeZhuo
 * @date 2020/12/10
 */
public class TestCurrentClass {

    public static void main(String[] args) {
        //线程测试工具类
        ConcurrencyTester concurrencyTester = new ConcurrencyTester(10);
        concurrencyTester.test(() -> {
            // 需要并发测试的业务代码
            System.out.println(Thread.currentThread() + "1");
        });
        System.out.println(concurrencyTester.getInterval());
    }
}
